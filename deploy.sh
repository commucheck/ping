#!/bin/bash

# Colors: http://misc.flogisoft.com/bash/tip_colors_and_formatting
# echo -e "\033[1;31mThis is red text\033[0m"
# echo -e "\033[1;32mThis is green text\033[0m"

set -o errexit # Exit on error

# Lint
echo -e "\033[1;32mRunning ESLint...\033[0m"
if npm run lint >> /dev/null; then
  echo -e "\033[1;32mNo linting errors\033[0m"
else
  echo -e "\033[1;31mESLint failed! Run npm run lint to troubleshoot\033[0m"
  exit 1
fi

# Run tests
echo -e "\033[1;32mRunning unit tests...\033[0m"
if npm test >> /dev/null; then
  echo -e "\033[1;32mAll tests succeeded\033[0m"
else
  echo -e "\033[1;31mTests failed! Run npm test to troubleshoot\033[0m"
  exit 1
fi

# git push

# if $(git commit -am Deploy); then # Commit the changes, if any
#   echo 'Changes Committed'
# fi

# Shippable will run tests again, build & push docker image
