FROM pierrickl/alpine-node-git-curl-ssh
MAINTAINER Pierrick Lozach

# Create app directory,
RUN mkdir -p /app
WORKDIR /app

# Get source code
# RUN git clone https://PierrickI3@bitbucket.org/commucheck/ping.git . --quiet

COPY . /app

# Install dependencies
RUN npm install

# Service port to expose
EXPOSE 3000

# Start service
CMD [ "npm", "start" ]