[![Run Status](https://api.shippable.com/projects/57dfa749f26de50f0073be49/badge?branch=master)](https://api.shippable.com/projects/57dfa749f26de50f0073be49)
[![Coverage](https://api.shippable.com/projects/57dfa749f26de50f0073be49/coverageBadge?branch=master)](https://api.shippable.com/projects/57dfa749f26de50f0073be49)


# PING #

This code is used to ping a target (machine, url) from a micro-service.
Uses kafka to process messages. Can be disabled in the POST request.

Docker image: [https://hub.docker.com/r/pierrickl/docker-ping/](https://hub.docker.com/r/pierrickl/docker-ping/)

### Usage ###
* Basic usage: [http://localhost:3000/api/v1/ping?host=<host>](), i.e. `http://localhost:3000/api/v1/ping?host=www.google.com`
* Specify port (optional, 80 by default): [http://localhost:3000/api/v1/ping?host=<host>&port=<port>](), i.e. `http://localhost:3000/api/v1/ping?host=www.google.com&port=80`
* Other optional parameters:
    * timeout: in milliseconds. Default value: 500
    * attempts: number of parallel attempts. Default value: 10
    * log: if set to false, will not output results to stdout. Default value: true
    * event: if set to false, will not send a message using message queueing (i.e. kafka). Default value: true

### How do I get set up? ###

* Clone this repo
* run `npm install`
* run `npm start`
* Optionally, run `npm test` to run tests