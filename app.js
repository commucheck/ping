var express = require('express');
var bodyParser = require('body-parser');

var api = require('./routes/api');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Handle CTRL-C and all other exit types
/* istanbul ignore next */
['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 'SIGBUS',
  'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGPIPE', 'SIGTERM'].forEach(function(element) {
  process.on(element, function() {
    console.log('\n$s: Gracefully shutting down', element);
    // Nothing to clean so far (kafka maybe?)
    process.exit(0);
  });
});

module.exports = app;
