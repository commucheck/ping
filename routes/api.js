"use strict";

var router = require("express").Router(); // eslint-disable-line new-cap
var tcpp = require("tcp-ping");
var log = require("../lib/log");
var kafka = require("../lib/kafka");
var moment = require("moment-timezone");

var topic = "commucheck";

/* GET */
router.get("/v1/ping", function(req, res) {

  // Validate params
  if (!req.query.host) {
    log.log(req, null, null, null, null, null, null, "No host specified");
    var err = new Error("No host specified");
    res.status(400); // Bad Request
    res.json({ message: err.message, error: err });
    return;
  }

  var host = req.query.host;

  // Set default values if variables are not set
  var port = req.query.port || 80;
  var timeout = req.query.timeout || 500;
  var attempts = req.query.attempts || 10;

  // Action!
  tcpp.ping({
    address: host,
    port: port,
    timeout: parseInt(timeout, 10),
    attempts: attempts
  },
  function(pingerr, data) {
    // Got error? Only happens if network connection goes down
    /* istanbul ignore next */
    if (pingerr) {
      console.log(pingerr);
      // Log result
      log.log(req, host, port, attempts, null, null, null, null, pingerr.message);
      
      //TODO Log error to kafka
      sendToKafka(req, topic, data.address, data.port, data.attempts, null, null, null, pingerr.message);

      // Return result
      res.status(pingerr.status || 500);
      res.json({ message: pingerr.message, error: pingerr });
    }

    // Check results
    if (data.attempts && isNaN(data.avg)) {
      // Invalid host or port
      log.log(req, data.address, data.port, data.attempts, data.avg, data.max, data.min, "Invalid host or port");

      // Log error to kafka
      sendToKafka(req, topic, data.address, data.port, data.attempts, data.avg, data.max, data.min, "Invalid host or port");

      res.status(404);
      res.json({ message: "Invalid host (" + data.address + ") or port (" + data.port + ")" });
    } else {
      // Success
      log.log(req, data.address, data.port, data.attempts, data.avg, data.max, data.min, null);
      
      // Send result to kafka
      sendToKafka(req, topic, data.address, data.port, data.attempts, data.avg, data.max, data.min, null);
      
      res.send(data);
    }
  });
});

var sendToKafka = function(req, topic, address, port, attempts, avg, max, min, error) {
  kafka.sendPayload(req, topic, [{
    topic: topic,
    partition: 0,
    messages: [
      // all messages must be strings
      JSON.stringify({
        timestamp: moment().unix(),
        host: address,
        port: port,
        attempts: attempts,
        avg: isNaN(avg) ? null : avg,
        max: max,
        min: min,
        error: error,
        url: req.originalUrl
      })
    ],
    attributes: 0
  }]);
}

module.exports = router;
