"use strict";

var request = require("supertest");

describe("Starting tests...", function() {

  var server;
  beforeEach(function() {
    server = require("../app");
  });

  describe("Ping e2e tests:", function() {

    describe("Successful tests:", function() {
      it("can ping", function(done) {
        request(server)
          .get("/api/v1/ping?host=www.google.com")
          .expect(200, done);
      });
      it("can ping without logging to stdout", function(done) {
        request(server)
          .get("/api/v1/ping?host=www.google.com&log=false")
          .expect(200, done);
      });
      it("can ping without sending event to kafka", function(done) {
        request(server)
          .get("/api/v1/ping?host=www.google.com&event=false")
          .expect(200, done);
      });
    });

    describe("Failing tests:", function() {
      it("400 (Bad Request) if no host provided", function(done) {
        request(server)
          .get("/api/v1/ping")
          .expect(400, done);
      });
      it("invalid host", function(done) {
        request(server)
          .get("/api/v1/ping?host=invalidmachine")
          .expect(404, done);
      });
      it("invalid port", function(done) {
        request(server)
          .get("/api/v1/ping?host=www.google.com&port=81")
          .expect(404, done);
      }).timeout(5100); // Default timeout is 500 ms per attempt (10 by default) so use 5000 minimum
      it("invalid port & longer timeout", function(done) {
        request(server)
          .get("/api/v1/ping?host=www.google.com&port=81&timeout=1000")
          .expect(404, done);
      }).timeout(10100); // 10 attempts are being made so default timeout is 10 times the value of timeout passed in the request;
    });
  });
});
