#!/bin/bash

docker build -t commucheck-ping .
docker tag commucheck-ping pierrickl/commucheck-ping:0.0.4
docker push pierrickl/commucheck-ping:0.0.4