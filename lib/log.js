"use strict";

var moment = require("moment-timezone");

var log = function(req, host, port, attempts, avg, max, min, error) {
  if (typeof req.query.log !== "undefined" && req.query.log !== null && req.query.log === "false") {
    return;
  }

  var timestamp = moment().unix();

  // log to stdout
  console.log(
    "ping! Date: %s, Host: %s, Port: %s, Attempts: %s, Avg: %d, Max: %d, Min: %d, Error: %s, Request: %s",
    timestamp,
    host,
    port,
    attempts,
    isNaN(avg) ? null : avg,
    max,
    min,
    error,
    req.originalUrl
  );
}

module.exports.log = log;
