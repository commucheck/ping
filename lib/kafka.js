"use strict";

var zookeeper = "zookeeper";
var zookeeperPort = 2181;

var kafkanode = require("kafka-node");

var Producer = kafkanode.Producer,
  client = new kafkanode.Client(zookeeper + ":" + zookeeperPort + "/"),
  producer = new Producer(client, { requireAcks: 1 });

var exports = module.exports = {};

var sendPayload = function(req, topic, payload) {
  
  // Do not send to kafka if event == false
  if (typeof req.query.event !== "undefined" && req.query.event !== null && req.query.event === "false") {
    return;
  }

  // Create kafka topic if it does not exist
  producer.createTopics([topic], false, function(err, data) {
    if (err) {
      console.log("create topics err:", err);
    }
    console.log("create topics data:", data);
  });

  // Only works in production
  /* istanbul ignore next */
  producer.send(payload, function(err, data) {
    if (err) {
      console.log("Error while sending payload to kafka:", err, ". data:", data);
    } else {
      console.log("Payload sent to kafka");
    }
  });
}

// Only happens in production
/* istanbul ignore next */
producer.on("ready", function() {
  console.log("Kafka producer ready");
});

// Only happens in production
/* istanbul ignore next */
producer.on("error", function(err) {
  console.log("Kafka producer error:", err);
});

module.exports = {
  sendPayload: sendPayload
}